from drb.core.factory import FactoryLoader
from drb.core.item_class import ItemClassLoader, ItemClassType
from drb.drivers.file import DrbFileFactory
from drb.nodes.logical_node import DrbLogicalNode

import unittest
import uuid


class TestDrbFileSignature(unittest.TestCase):
    mock_pkg = None
    fc_loader = None
    ic_loader = None
    file_id = uuid.UUID('99e6ce18-276f-11ec-9621-0242ac130002')

    @classmethod
    def setUpClass(cls) -> None:
        cls.fc_loader = FactoryLoader()
        cls.ic_loader = ItemClassLoader()

    def test_driver_loading(self):
        factory_name = 'file'
        factory = self.fc_loader.get_factory(factory_name)
        self.assertIsNotNone(factory)
        self.assertIsInstance(factory, DrbFileFactory)

    def test_topic_loading(self):
        item_class = self.ic_loader.get_item_class(self.file_id)
        self.assertIsNotNone(item_class)
        self.assertEqual(self.file_id, item_class.id)
        self.assertEqual('file', item_class.label)
        self.assertIsNone(item_class.description)
        self.assertEqual(ItemClassType.PROTOCOL, item_class.category)
        self.assertEqual(self.fc_loader.get_factory('file'),
                         item_class.factory)

    def test_topic_signatures(self):
        item_class = self.ic_loader.get_item_class(self.file_id)

        node = DrbLogicalNode('.')
        self.assertTrue(item_class.matches(node))

        node = DrbLogicalNode('https://gitlab.com/drb-python')
        self.assertFalse(item_class.matches(node))
