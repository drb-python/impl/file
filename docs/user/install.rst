.. _install:

Installation of file implementation
====================================
Installing ``drb-driver-file`` with execute the following in a terminal:

.. code-block::

   pip install drb-driver-file
