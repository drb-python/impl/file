.. _examples:

Examples:
=========

Read a file with drb
----------------------
.. literalinclude:: example/read_file.py
    :language: python

Move in a directory with drb
-----------------------------
.. literalinclude:: example/directory.py
    :language: python
