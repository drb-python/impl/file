from drb.drivers.file import DrbFileNode, DrbFileAttributeNames

dir_path = 'test'

# Connect with no credential
node = DrbFileNode(dir_path)

# List the contents of a folder
[e.name for e in node.children]

# Check if one of the sub folder is a directory
key = DrbFileAttributeNames.DIRECTORY.value
node['subFolderName'].get_attribute(key)
