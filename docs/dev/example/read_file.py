import io

from drb.drivers.file import DrbFileNode, DrbFileAttributeNames

file_path = '/path/to/your/file'

# Connect with no credential
node = DrbFileNode(file_path)

# The name of the file
node.name

# The parsed path of the file
node.path

# All the attributes of the file
node.attributes

# Check if the file is a directory
key = DrbFileAttributeNames.DIRECTORY.value
node.get_attribute(key)

# Get the content of a file as stream
with node.get_impl(io.RawIOBase) as stream:
    stream.read()
