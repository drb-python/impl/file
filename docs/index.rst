===================
Data Request Broker
===================
---------------------------------
FILE implementation for DRB
---------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-impl-file/month
    :target: https://pepy.tech/project/drb-impl-file
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-impl-file.svg
    :target: https://pypi.org/project/drb-impl-file/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-impl-file.svg
    :target: https://pypi.org/project/drb-impl-file/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-impl-file.svg
    :target: https://pypi.org/project/drb-impl-file/
    :alt: Python Version Support Badge

-------------------

This drb-impl-file module implements file protocol access with DRB data model.
It is able to navigates among the file systems.

User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example

