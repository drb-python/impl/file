from drb.exceptions.core import DrbException, DrbFactoryException


class DrbFileNodeException(DrbException):
    pass


class DrbFileNodeFactoryException(DrbFactoryException):
    pass
